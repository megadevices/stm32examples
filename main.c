#include "stm32f10x.h"                  // Device header

#define BAUDRATE 9600

void Init_USART1()
{
	

	
	// configure usart1
  USART_InitTypeDef uart_struct;
  uart_struct.USART_BaudRate            = BAUDRATE;
  uart_struct.USART_WordLength          = USART_WordLength_8b;
  uart_struct.USART_StopBits            = USART_StopBits_1;
  uart_struct.USART_Parity              = USART_Parity_No;
  uart_struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  uart_struct.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART1, &uart_struct);
  USART_Cmd(USART1, ENABLE);
  
	// enable usart1 interrupt
//  NVIC_InitTypeDef NVIC_InitStructure;
//  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);


	
	/*
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
	    // enable usart1 interrupt
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    NVIC_EnableIRQ(TIM2_IRQn);
		
		void uart_send_byte(uint8_t data) {
    while(!(USART1->SR & USART_SR_TC)); //Wait for transmit ready
    USART_SendData(USART1, data);
}
void uart_print(char* str){
    while(*str){
        uart_send_byte(*str);
        str++;
    }
}
void uart_println(char* str){
    uart_print(str);
    uart_print("\n\r");
}
		
		
	*/
	
	
}


//Interrupt handler
//void USART1_IRQHandler(void)
//{
//    //Receive Data register not empty interrupt
//    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
//    {
//        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
//        uint8_t dr=USART_ReceiveData(USART1) - '0';
//        uart_print("leds on: ");
//        if (dr & 1) {
//            GPIOB->ODR |= GPIO_Pin_4;
//            uart_print("white ");
//        } else {
//            GPIOB->ODR &= ~GPIO_Pin_4;
//        }
//        if (dr & 2) {
//            GPIOB->ODR |= GPIO_Pin_5;
//            uart_print("blue");
//        } else {
//            GPIOB->ODR &= ~GPIO_Pin_5;
//        }
//        uart_println("");
//    }
//}



//void Init_USART1(){
//     
//				USART_InitTypeDef               USART_InitStructure;
//        USART_InitStructure.USART_BaudRate = 9600;
//        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
//        USART_InitStructure.USART_Mode =  USART_Mode_Rx | USART_Mode_Tx;
//        USART_InitStructure.USART_Parity = USART_Parity_No;
//        USART_InitStructure.USART_StopBits = USART_StopBits_1;
//        USART_InitStructure.USART_WordLength = USART_WordLength_8b;
//        USART_Init(USART1,&USART_InitStructure);
//         
//        USART_ClockStructInit(&USART_Clk_InitStructure);
//        USART_ClockInit(USART1,&USART_Clk_InitStructure);
// 
//}

//void GPIO_Configuration(void) 
//{
//	GPIO_InitTypeDef GPIO_InitStructure;

//	
//	
//		/* USART1 Tx (PA9) */
//  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
//  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	 		//复用开漏输出模式
//  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		//输出最大频率为50MHz
//  	GPIO_Init(GPIOA, &GPIO_InitStructure);
//}

int main (void)
{
	while(SysTick_Config(SystemCoreClock / 1000));
	// Enable clock of modules
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	Init_USART1();
	USART_SendData(USART1, 'H');
	
	while(1)
	{
		
	}
	
}

